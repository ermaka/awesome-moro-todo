# Awesome Moro TODO application

A simple frontend application that allows to create, edit or delete tasks.
Users can also change the status of tasks, marking them as completed or incompleted.
It is possible to edit a text of tasks by double-clicking the text. All completed tasks can be deleted immediately.
GUI is made using [React.js](https://reactjs.org/) framework with [Redux](https://redux.js.org) and [Redux Saga](https://redux-saga.js.org/).
This application requires the specific backend part you can find [here](https://github.com/morosystems/todo-be).

### Prerequisites
- [Node](https://nodejs.org)
- [NPM](https://www.npmjs.com)

### Build
```bash
npm install
```
To compile optimized production build run
```bash
npm build
```

### Run
To run the application in development mode
```bash
npm dev
```
Application is available at `http://localhost:3000`.

### Docker
You can dockerize this application by command
```bash
docker build -t awesome-moror-todo:latest -f docker/Dockerfile-front .
```
It builds a new production-ready [Docker](https://www.docker.com/) image named `awesome-moror-todo:latest`. 
Internally the production build is served by Nginx server. Create and run a Docker container with the application, expose port 80.
```bash
docker run -p 80:80 awesome-moror-todo:latest ...
```

You can also build a Docker image for the backend part. By the command below, Docker will download backend code from upstream,
unzip and compile it and create a Docker image named `awesome-moro-todo-be:latest`.
```bash
docker build -t awesome-moro-todo-be:latest -f docker/Dockerfile-back .
```

Use the attached `docker-compose.yml` to run both frontend and backend parts.
```bash
docker-compose -f docker/docker-compose.yml up -d
```

### Licence
[GPL v3](https://www.gnu.org/licenses/gpl-3.0)

### Author
[Aleksei Ermak](https://github.com/kazooo)