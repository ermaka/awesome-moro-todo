import React from "react";
import {NewTaskForm} from "../new-task-form/NewTaskForm";
import {CompleteAll} from "../complete-all/CompleteAll";
import {Card, Col, Container, Row} from "react-bootstrap";

export const Header = () => (
    <Card.Header>
        <Container>
            <Row>
                <Col xs lg="1">
                    <CompleteAll />
                </Col>
                <Col>
                    <NewTaskForm />
                </Col>
            </Row>
        </Container>
    </Card.Header>
);
