import React from "react";
import {useState} from "react";
import {useDispatch} from "react-redux";
import {addTask} from "../../../redux/actions/saga-triggers";
import {Form} from "react-bootstrap";
import "./NewTaskForm.css";

export const NewTaskForm = () => {

    const dispatch = useDispatch();
    const [taskText, setTaskText] = useState("");

    const handleOnChange = (e) => {
        setTaskText(e.target.value);
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter'){
            e.preventDefault();
            dispatch(addTask({text: taskText}));
            setTaskText("");
        }
    }

    return <Form className="NewTaskForm">
        <Form.Control
            type="text"
            size="sm"
            value={taskText}
            onChange={handleOnChange}
            onKeyDown={handleKeyDown}
            placeholder="Your task text"
        />
        <Form.Text muted>
            Press Enter to add new task.
        </Form.Text>
    </Form>;
}
