import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {switchStateAll} from "../../../redux/actions/saga-triggers";
import {allTasksAreCompleted} from "../../../redux/selectors";
import "./CompleteAll.css";

export const CompleteAll = () => {

    const dispatch = useDispatch();
    const checked = useSelector(state => allTasksAreCompleted(state));

    const handleOnChange = () => {
        dispatch(switchStateAll(checked));
    }

    return <div className="CompleteAll">
        <input
            type="checkbox"
            checked={checked}
            onChange={handleOnChange}
        />
    </div>;
};
