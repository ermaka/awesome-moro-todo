import {useDispatch, useSelector} from "react-redux";
import {deleteCompleted} from "../../../redux/actions/saga-triggers";
import {getCompletedTasks} from "../../../redux/selectors";
import {Button} from "react-bootstrap";

export const ClearCompleted = () => {

    const dispatch = useDispatch();
    const completedTasks = useSelector(state => getCompletedTasks(state));

    const handleOnClick = () => {
        dispatch(deleteCompleted());
    }

    return completedTasks.length > 0 &&
        <div>
            <Button
                size="sm"
                onClick={handleOnClick}
                variant="outline-danger"
            >Clear completed</Button>
        </div>;
}
