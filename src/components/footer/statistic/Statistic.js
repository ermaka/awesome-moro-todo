import React from "react";
import {useSelector} from "react-redux";
import {Row} from "react-bootstrap";
import {getTasks} from "../../../redux/selectors";
import "./Statistic.css";

export const Statistic = () => {

    const tasks = useSelector(state => getTasks(state));

    return <Row className="Statistic">
        <div>All: {tasks.length}</div>
        <div>Active: {tasks.filter(task => task.completed === false).length}</div>
        <div>Completed: {tasks.filter(task => task.completed === true).length}</div>
    </Row>;
}
