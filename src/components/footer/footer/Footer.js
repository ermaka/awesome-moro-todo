import React from "react";
import {Statistic} from "../statistic/Statistic";
import {ClearCompleted} from "../clear-completed/ClearCompleted";
import {Filter} from "../filter/Filter";
import {Card, Col, Row} from "react-bootstrap";

export const Footer = () => (
    <Card.Footer className="text-muted">
        <Row>
            <Col>
                <Statistic />
            </Col>
            <Col md="auto">
                <Filter />
            </Col>
            <Col>
                <ClearCompleted />
            </Col>
        </Row>
    </Card.Footer>
);
