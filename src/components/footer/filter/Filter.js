import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {changeFilter} from "../../../redux/slices/global";
import {getVisibilityFilter} from "../../../redux/selectors";
import {ACTIVE, ALL, COMPLETED} from "../../../constants/filter-types";
import {Button} from "react-bootstrap";
import "./Filter.css";

export const Filter = () => {

    const filter = useSelector(state => getVisibilityFilter(state));
    const dispatch = useDispatch();

    const setFilter = (newFilter) => {
        dispatch(changeFilter(newFilter));
    }

    return <div className="Filter">
        <Button variant="outline-warning" size="sm" active={filter === ALL}
                onClick={() => setFilter(ALL)}>All</Button>
        <Button variant="outline-warning" size="sm" active={filter === ACTIVE}
                onClick={() => setFilter(ACTIVE)}>Active</Button>
        <Button variant="outline-warning" size="sm" active={filter === COMPLETED}
                onClick={() => setFilter(COMPLETED)}>Completed</Button>
    </div>
}
