import store from "../redux/store";
import {App} from "./app/App";
import ReduxToastr from "react-redux-toastr";
import {Provider} from "react-redux";
import React from "react";

export const Root = () => (
    <Provider store={store}>
        <App />
        <ReduxToastr
            timeOut={6000}
            newestOnTop={true}
            position="bottom-right"
            getState={(state) => state.toastr}
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            progressBar={false}
            closeOnToastrClick
        />
    </Provider>
)
