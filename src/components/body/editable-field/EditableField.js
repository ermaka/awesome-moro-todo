import {useEffect, useRef, useState} from "react";
import {Form} from "react-bootstrap";

const EditableField = ({value, callback, submit}) => {

    const [text, setText] = useState(value);

    function useOutsideHandler(ref) {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target))
                    callback();
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }
    const wrapperRef = useRef(null);
    useOutsideHandler(wrapperRef);

    const handleOnChange = (e) => {
        setText(e.target.value);
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            submit(text);
            callback();
        }
        else if (e.key === 'Escape') {
            callback();
        }
    }

    return <Form>
        <Form.Control
            type="text"
            size="sm"
            value={text}
            ref={wrapperRef}
            onChange={handleOnChange}
            onKeyDown={handleKeyDown}
            placeholder="Your task text"
        />
        <Form.Text muted>
            Press Enter to submit new task text or Esc to cancel editing.
        </Form.Text>
    </Form>;
}

export default EditableField;
