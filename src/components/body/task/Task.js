import {completeTask, deleteTask, editTask, incompleteTask} from "../../../redux/actions/saga-triggers";
import {Button, Col, Row} from "react-bootstrap";
import EditableField from "../editable-field/EditableField";
import {useDispatch} from "react-redux";
import {useState} from "react";
import "./Task.css";

export const Task = ({info}) => {

    const [edit, setEdit] = useState(false);
    const dispatch = useDispatch();

    const handleOnChange = () => {
        if (info.completed)
            dispatch(incompleteTask(info));
        else
            dispatch(completeTask(info));
    }

    const handleOnSubmit = (e) => {
        e.preventDefault();
        dispatch(deleteTask(info));
    }

    const handleNewTaskSubmit = (text) => {
       dispatch(editTask({id: info.id, text: text}));
    }

    const turnEditIfNotCompleted = () => {
        if (!info.completed)
            setEdit(true)
    }

    return <Row className="Task">
        <Col xs lg="1">
            <input
                type="checkbox"
                checked={info.completed}
                onChange={handleOnChange}
            />
        </Col>
        <Col>
            {edit ?
                <EditableField
                    value={info.text}
                    callback={() => setEdit(false)}
                    submit={handleNewTaskSubmit}
                />
                :
                <div onDoubleClick = {turnEditIfNotCompleted}
                     className={info.completed ? "completed-text" : ""}>
                    {info.text}
                </div>
            }
        </Col>
        <Col xs lg="2">
            <Button
                size="sm"
                variant="outline-danger"
                disabled={edit}
                onClick={handleOnSubmit}
            >Delete</Button>
        </Col>
    </Row>;
};
