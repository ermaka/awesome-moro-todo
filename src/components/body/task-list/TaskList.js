import {useDispatch, useSelector} from "react-redux";
import {Task} from "../task/Task";
import React, {useEffect} from "react";
import {getTasks} from "../../../redux/actions/saga-triggers";
import {getIsLoading, getVisibleTasks} from "../../../redux/selectors";
import {Card, Container, Spinner} from "react-bootstrap";
import "./TaskList.css";

export const TaskList = () => {

    const dispatch = useDispatch();
    const isLoading = useSelector(state => getIsLoading(state));
    const filteredTasks = useSelector(state => getVisibleTasks(state));

    useEffect(() => {dispatch(getTasks())}, [dispatch]);

    if (isLoading) {
        return <Card.Body><Spinner animation="grow" /></Card.Body>;
    }

    return <Card.Body>
            {filteredTasks.length > 0 ?
                <Container className="TaskList">
                    {filteredTasks.map(task => {
                        return <div key={task.id}><Task info={task} /></div>
                    })}
                </Container>
                :
                <div>No tasks to display</div>
            }
        </Card.Body>;
};
