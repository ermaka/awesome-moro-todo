import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import React from "react";
import {render} from "react-dom";
import {Root} from "./components/Root";

render(
    <Root />,
    document.getElementById("root")
);
