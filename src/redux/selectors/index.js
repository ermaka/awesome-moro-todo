import {createSelector} from "reselect";
import {ACTIVE, COMPLETED} from "../../constants/filter-types";

export const getTasks = state => state.tasks.list;

export const getIsLoading = state => state.global.isLoading;

export const getVisibilityFilter = state => state.global.filter;

export const getCompletedTasks = createSelector(
    [getTasks],
    (tasks) => {
        return tasks.filter(task => task.completed);
    }
);

export const getVisibleTasks = createSelector(
    [getVisibilityFilter, getTasks],
    (filter, tasks) => {
        switch (filter) {
            case COMPLETED:
                return tasks.filter(t => t.completed);
            case ACTIVE:
                return tasks.filter(t => !t.completed);
            default:
                return tasks;
        }
    }
);

export const allTasksAreCompleted = createSelector(
    [getTasks, getCompletedTasks],
    (tasks, completedTasks) => {
        return tasks.length > 0 && tasks.length === completedTasks.length;
    }
);
