import {toastr} from 'react-redux-toastr'
import {API_ERRORED, TASK_ADDED} from "../../constants/action-types";
import {createAction} from "@reduxjs/toolkit";

export const apiErrored = createAction(API_ERRORED);

export function toastReactionMiddleware() {
    return function(next) {
        return function(action) {
            switch (action.type) {
                case API_ERRORED:
                    toastr.error(action.payload);
                    break;
                case TASK_ADDED:
                    toastr.success("Successfully added \"" + action.payload.text + "\" task!");
                    break;
                default:
                    break;
            }
            return next(action);
        };
    };
}
