import {createAction} from "@reduxjs/toolkit";
import {
    ADD_TASK_REQUESTED,
    SWITCH_STATE_ALL_REQUESTED,
    DELETE_COMPLETED_REQUESTED,
    DELETE_TASK_REQUESTED,
    EDIT_TASK_REQUESTED,
    TASK_COMPLETE_REQUESTED,
    TASK_INCOMPLETE_REQUESTED,
    TASKS_REQUESTED
} from "../../constants/action-types";

export const getTasks = createAction(TASKS_REQUESTED);
export const addTask = createAction(ADD_TASK_REQUESTED);
export const editTask = createAction(EDIT_TASK_REQUESTED);
export const completeTask = createAction(TASK_COMPLETE_REQUESTED);
export const incompleteTask = createAction(TASK_INCOMPLETE_REQUESTED);
export const deleteTask = createAction(DELETE_TASK_REQUESTED);
export const deleteCompleted = createAction(DELETE_COMPLETED_REQUESTED);
export const switchStateAll = createAction(SWITCH_STATE_ALL_REQUESTED);
