import superagent from "superagent";
import superagentAbsolute from "superagent-absolute";

const agent = superagent.agent();
export const request = superagentAbsolute(agent)(window._env_.BACKEND_API_URL);
