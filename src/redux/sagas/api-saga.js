import {taskAdded, taskDeleted, tasksLoaded, taskUpdated} from "../slices/tasks";
import {takeEvery, call, put, select, all} from "redux-saga/effects";
import {apiErrored} from "../middlewares/toastr-middleware";
import {getCompletedTasks, getTasks} from "../selectors";
import {setIsLoading} from "../slices/global";
import {request} from "./saga-config";
import {
    ADD_TASK_REQUESTED,
    SWITCH_STATE_ALL_REQUESTED,
    DELETE_COMPLETED_REQUESTED,
    DELETE_TASK_REQUESTED,
    EDIT_TASK_REQUESTED,
    TASK_COMPLETE_REQUESTED,
    TASK_INCOMPLETE_REQUESTED,
    TASKS_REQUESTED
} from "../../constants/action-types";

export default function* watcherSaga() {
    yield takeEvery(TASKS_REQUESTED, getAllTasksSaga);
    yield takeEvery(ADD_TASK_REQUESTED, addTaskSaga);
    yield takeEvery(EDIT_TASK_REQUESTED, editTaskSaga);
    yield takeEvery(TASK_COMPLETE_REQUESTED, completeTaskSaga);
    yield takeEvery(TASK_INCOMPLETE_REQUESTED, incompleteTaskSaga);
    yield takeEvery(DELETE_TASK_REQUESTED, deleteTaskSaga);
    yield takeEvery(DELETE_COMPLETED_REQUESTED, deleteCompletedSaga);
    yield takeEvery(SWITCH_STATE_ALL_REQUESTED, switchStateAll);
}

function* getAllTasksSaga() {
    try {
        yield put(setIsLoading(true));
        const payload = yield call(() => request.get("/todos"));
        yield put(tasksLoaded(payload.body));
        yield put(setIsLoading(false));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't fetch tasks from backend!"));
    }
}

function* addTaskSaga(action) {
    try {
        const payload = yield call(() => request
                .post("/todos")
                .send({ text: action.payload.text })
        );
        yield put(taskAdded(payload.body));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't add \"" + action.payload.text + "\" task!"));
    }
}

function* editTaskSaga(action) {
    try {
        const payload = yield call(() => request
                .post("/todos/" + action.payload.id)
                .send({text: action.payload.text})
        );
        yield put(taskUpdated(payload.body));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't update \"" + action.payload.text + "\" task!"));
    }
}

function* completeTaskSaga(action) {
    try {
        const payload = yield call(() => request.post("/todos/" + action.payload.id + "/complete"));
        yield put(taskUpdated(payload.body));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't complete \"" + action.payload.text + "\" task!"));
    }
}

function* incompleteTaskSaga(action) {
    try {
        const payload = yield call(() => request.post("/todos/" + action.payload.id + "/incomplete"));
        yield put(taskUpdated(payload.body));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't incomplete \"" + action.payload.text + "\" task!"));
    }
}

function* deleteTaskSaga(action) {
    try {
        yield call(() => request.delete("/todos/" + action.payload.id));
        yield put(taskDeleted(action.payload));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't delete \"" + action.payload.text + "\" task!"));
    }
}

function* deleteCompletedSaga(action) {
    try {
        yield put(setIsLoading(true));
        const completedTasks = yield select(getCompletedTasks);
        yield all(completedTasks.map(task => call(deleteTaskSaga, {payload: task})));
        yield put(setIsLoading(false));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't delete completed tasks!"));
    }
}

function* switchStateAll(action) {
    try {
        const checked = action.payload;
        const tasks = yield select(getTasks);
        yield all(tasks.map(task => {
            if (!checked && !task.completed)
                return call(completeTaskSaga, {payload: task});
            else if (checked && task.completed)
                return call(incompleteTaskSaga, {payload: task});
        }));
    } catch (e) {
        console.error(e);
        yield put(apiErrored("Can't complete/incomplete all tasks!"))
    }
}
