import {configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import {toastReactionMiddleware} from "./middlewares/toastr-middleware";
import {reducer as toastr} from 'react-redux-toastr'
import createSagaMiddleware from "redux-saga";
import apiSaga from "./sagas/api-saga";

import taskSlice from "./slices/tasks";
import globalSlice from "./slices/global";

const reducers = {
    tasks: taskSlice.reducer,
    global: globalSlice.reducer,
    toastr: toastr
}

const initialSagaMiddleware = createSagaMiddleware();

const middlewares = [
    ...getDefaultMiddleware({thunk: false}),
    initialSagaMiddleware,
    toastReactionMiddleware
]

const store = configureStore({
    reducer: reducers,
    middleware: middlewares
});

initialSagaMiddleware.run(apiSaga);

export default store;
