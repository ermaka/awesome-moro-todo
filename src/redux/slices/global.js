import {createSlice} from "@reduxjs/toolkit";
import {ALL} from "../../constants/filter-types";

const globalSlice = createSlice({
    name: "global",
    initialState: {
        filter: ALL,
        isLoading: true
    },
    reducers: {
        changeFilter: (state, action) => {
            state.filter = action.payload;
        },
        setIsLoading: (state, action) => {
            state.isLoading = action.payload;
        }
    }
});

export const {changeFilter, setIsLoading} = globalSlice.actions;
export default globalSlice;
