import {createSlice} from "@reduxjs/toolkit";

const taskSlice = createSlice({
    name: "tasks",
    initialState: {
        list: []
    },
    reducers: {
        tasksLoaded: (state, action) => {
            state.list = action.payload;
            state.isLoading = false;
        },
        taskAdded: (state, action) => {
            state.list = state.list.concat(action.payload);
        },
        taskUpdated: (state, action) => {
            state.list = state.list.map(task => task.id === action.payload.id ? action.payload : task);
        },
        taskDeleted: (state, action) => {
            state.list = state.list.filter(task => task.id !== action.payload.id);
        }
    }
});

export const {tasksLoaded, taskAdded, taskUpdated, taskDeleted} = taskSlice.actions;
export default taskSlice;
